module.exports = (message) => {
	if (message !== "!help") {
		return false;
	}
	return "My commands: ```!who - give info about myself" +
                            "\n!help - give list of my commands" +
                            "\n!shuffle [line of characters] - shuffles all of the characters in the line```";
};