//get commit hash
var hash;
require("child_process").exec("git rev-parse HEAD", function(err, stdout){
	hash = stdout.slice(0,7);
});

module.exports = (message) => {
	if (message !== "!who") {
		return false;
	}
	return "My creator is Siim Tänavots. Code at https://gitlab.com/Si1mT/ta-17e-discord-bot . Last commit hash: " + hash + ". Initiating Handshake protocol...";
};