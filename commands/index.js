const who = require("./who");
const help = require("./help");
const shuffle = require("./shuffle");

module.exports = [
	who,
	help,
	shuffle
];
