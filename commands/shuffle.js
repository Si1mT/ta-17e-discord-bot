module.exports = (message) => {
	if(message.startsWith("!shuffle")){
		return message.substring(9).shuffle();
	}
	else{
		return false;
	}
};

// shuffle string method
String.prototype.shuffle = function () {
	var a = this.split(""),
		n = a.length;

	for(var i = n - 1; i > 0; i--) {
		var j = Math.floor(Math.random() * (i + 1));
		var tmp = a[i];
		a[i] = a[j];
		a[j] = tmp;
	}
	return a.join("");
};