const Discord = require("discord.js");
const { prefix, token } = require("./config.json");
const commands = require("./commands");
const cmd = require("node-cmd");
const express = require("express");
const http = require("http");
const client = new Discord.Client();
var currentTime = new Date();

client.once("ready", () => {
	console.log(client.user.tag + " Ready! Current time: " + currentTime);
	console.log("xd");
});

client.on("message", message => {
	if(message.author.bot){
		return;
	}
	Object.values(commands).forEach((command) => {
		const response = command(message.content);
		console.log("message: " + message.content);
		console.log("response: " + response);
		if (response) {
		  message.channel.send(response);
		}
	});
});

client.login(process.env.BOT_TOKEN);

const app = express();
app.get("/", (request, response) => {
	console.log(`${Date.now()} Ping Received`);
	response.sendStatus(200);
});
app.listen(process.env.PORT);
setInterval(() => {
	http.get(`http://${process.env.PROJECT_DOMAIN}.glitch.me/`);
}, 270000);
setInterval(() => {
	cmd.run("refresh");
}, 3600000);

app.post("/git", (req, res) => {
	if (req.headers["x-gitlab-event"] === "Push Hook") {
		cmd.run("chmod 777 commands/git.sh");
		cmd.get("commands/git.sh", (err, data) => {
			if (data) console.log(data);
			if (err) console.log(err);
		});
		cmd.run("refresh");

		console.log("> [GITLAB] Updated with gitlab/master");
	}
	return res.sendStatus(200);
});
